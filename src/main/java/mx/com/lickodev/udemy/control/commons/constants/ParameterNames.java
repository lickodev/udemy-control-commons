package mx.com.lickodev.udemy.control.commons.constants;

public class ParameterNames {


	private ParameterNames() {
	}

	public static final String PARAMETER_DESCRIPTION = "description";
	public static final String PARAMETER_NAME = "name";
	public static final String PARAMETER_USER_NAME = "userName";
	public static final String PARAMETER_ROLE_NAME = "roleName";
	public static final String PARAMETER_URL = "url";
	public static final String PARAMETER_ID = "id";
}
