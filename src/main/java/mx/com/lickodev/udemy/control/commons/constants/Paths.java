package mx.com.lickodev.udemy.control.commons.constants;

public class Paths {

	private Paths() {

	}
	
	public static final String SEARCH_PATH = "search";
	public static final String COURSES_BASE_PATH = "courses";
	public static final String USERS_BASE_PATH = "users";

	public static final String USERS_FIND_BY_USER_NAME_PATH = "by-user-name";
	public static final String USERS_FIND_BY_ID_PATH = "by-id";
	public static final String USERS_CONTAINING_USER_NAME_PATH = "containing-user-name";
	public static final String USERS_CONTAINING_ROLE_NAME_PATH = "containing-role-name";
	public static final String COURSES_EXISTS_CONTAINING_NAME_PATH = "exists-containing-name";
	public static final String USERS_SAVE_PATH = "save";
	public static final String COURSES_CONTAINING_NAME_PATH = "containing-name";
	public static final String COURSES_CONTAINING_NAME_OR_URL_OR_DESCRIPTION_PATH = "containing-name-url-or-description";
	public static final String COURSES_LIKE_NAME_OR_URL_OR_DESCRIPTION_PATH = "like-name-url-or-description";
	public static final String COURSES_BY_NAME_PATH = "by-name";


}