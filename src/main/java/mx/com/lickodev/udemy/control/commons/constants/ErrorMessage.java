package mx.com.lickodev.udemy.control.commons.constants;

public class ErrorMessage {

    public static final String LABEL_ERROR_DESCRIPTION_MIN_MAX = "error_description_min_max";

    public static final String LABEL_ERROR_NAME_EMPTY = "error_name_empty";
    public static final String LABEL_ERROR_NAME_MIN_MAX = "error_name_min_max";
    public static final String LABEL_ERROR_ID_NAME_MIN_MAX = "error_id_name_min_max";
    public static final String LABEL_ERROR_ROLE_NAME_MIN_MAX = "error_role_name_min_max";
    public static final String LABEL_ERROR_URL_MIN_MAX = "error_url_min_max";

    private ErrorMessage() {
    }

}
