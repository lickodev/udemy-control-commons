package mx.com.lickodev.udemy.control.commons.entity.users;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import static mx.com.lickodev.udemy.control.commons.constants.ParameterSizes.MAX_LENGTH_ROLE_NAME;

@Data
@Entity
@Table(name = "roles", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "roleName" }, name = "unique_role_role_name_constraint") })
public class Role implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(length = MAX_LENGTH_ROLE_NAME)
	@NotNull
	@Size(min = 8, max = MAX_LENGTH_ROLE_NAME)
	private String roleName;

}
