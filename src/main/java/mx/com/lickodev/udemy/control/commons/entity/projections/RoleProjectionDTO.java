package mx.com.lickodev.udemy.control.commons.entity.projections;

import mx.com.lickodev.udemy.control.commons.entity.users.Role;
import org.springframework.data.rest.core.config.Projection;

//@Value
@Projection(name = "roleProjection", types = Role.class)
public interface RoleProjectionDTO {

    Long getId();

    String getRoleName();

}
